import React, { useState } from "react";

const Panel = () => {
  const [selectedValue, setSelectedValue] = useState(-1);
  const [instances, setInstances] = useState([
    "W",
    "PTC",
    "LWC",
    "FCP",
    "YOI",
    "BTO",
    "KL",
    "TBO",
  ]);
  return (
    <div className="sidebar">
      <div onClick={()=>setInstances([...instances,"new"])} className="sidebar_item sidebar_add">+ Add</div>
      {instances.map((value, index) => (
        <div
          key={index}
          onClick={() => setSelectedValue(index)}
          className={
            "sidebar_item" +
            (index === selectedValue ? " sidebar_item--selected" : "")
          }
          type="text"
        >
          {value}
        </div>
      ))}
    </div>
  );
};

export default Panel;
