import React from 'react';


const Card = () => (
    <div className="card">
        <div className="card_title">
            <i className="fa fa-fire card_title-icon" aria-hidden="true"></i>
            FRAME
        </div>
        <div className="card_content">
            <div className="card_input">
                <div className="card_input-title">GOALS</div>

                <div className="card_input-area">
                    <div>
                        <div></div>
                        <div></div>
                        <div></div>
                    </div>
                    <textarea placeholder="Consequat officia veniam ut nulla ipsum. Occaecat esse fugiat ad do. Amet ullamco reprehenderit nisi amet tempor irure voluptate cillum labore sint laborum enim dolore. Irure eiusmod duis anim magna. Ea id ea irure laboris non elit." ></textarea>

                </div>
            </div>
            <div className="card_input">
                <div className="card_input-title">CONTROL</div>
                <div className="card_input-area">
                    <div>
                        <div></div>
                        <div></div>
                        <div></div>
                    </div>
                    <textarea placeholder="Consequat officia veniam ut nulla ipsum. Occaecat esse fugiat ad do. Amet ullamco reprehenderit nisi amet tempor irure voluptate cillum labore sint laborum enim dolore. Irure eiusmod duis anim magna. Ea id ea irure laboris non elit."></textarea>

                </div>

            </div>
            <div className="card_input">
                <div className="card_input-title">CHECKPOINT</div>

                <div className="card_input-area">
                    <div>
                        <div></div>
                        <div></div>
                        <div></div>
                    </div>
                    <textarea placeholder="Consequat officia veniam ut nulla ipsum. Occaecat esse fugiat ad do. Amet ullamco reprehenderit nisi amet tempor irure voluptate cillum labore sint laborum enim dolore. Irure eiusmod duis anim magna. Ea id ea irure laboris non elit." ></textarea>

                </div>
            </div>
            
        </div>
        <button className="card_save">SAVE</button>
    </div>
)


export default Card;