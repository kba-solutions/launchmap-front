import React from 'react';
import Panel from './Panel';
import Sidebar from './Sidebar';

const Home = () => (
  <div className="page-content">
    <Sidebar></Sidebar>
    <Panel></Panel>
  </div>
)
export default Home;